package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 商品別集計ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";
	
	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String BRANCH_FILE_ERROR = "支店定義ファイル";
	private static final String NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String OVER_10_DIGITS = "合計金額が10桁を超えました";
	private static final String BRANCH_CODE_NOT_EXIST = "の支店コードが不正です";
	private static final String RCDFILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String COMMODITY_FILE_ERROR = "商品定義ファイル";
	private static final String COMMODITY_CODE_NOT_EXIST = "の商品コードが不正です";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	
	
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が渡されているかどうかの確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		
		// 支店コードと支店名を保持するMap(支店コード、支店名)
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap(支店コード、売上金額)
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するmap(商品コード、商品名)
		Map<String, String>commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するmap(商品コード、売上金額)
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイルの読み込み処理
		String branchCodeRegex = "^[0-9]{3}";
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchCodeRegex, 
				BRANCH_FILE_ERROR)) {
			return;
		}
		//商品定義ファイルの読み込み処理
		String commodityCodeRegex = "^[A-Za-z0-9]{8}";
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST , commodityNames, commoditySales, commodityCodeRegex, 
				COMMODITY_FILE_ERROR)) {
			return;
		}

		//対象のディレクトリの中からrcdファイルを探してListに追加
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		
		for(int i = 0; i < files.length; i++) {
			//ファイルかどうか、かつ、「数字8桁.rcd」になっているかを確認して条件を満たすものだけrcdFilesに追加
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		
		//売上ファイルが連番かどうかの確認
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if(latter - former != 1) {
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}
		
		//rcdファイルのListからファイルを読み込んで、branchDataにその内容(支店コード、売上)を格納、
		//格納した情報がmapのkeyとvaluenに対応するようにする
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				//売上ファイルの読み込み
				br = new BufferedReader( new FileReader(rcdFiles.get(i)));
				//支店コード、売上額を抽出
				List<String> rcdData = new ArrayList<>();
				String line;
				while((line = br.readLine()) != null) {
					rcdData.add(line);
				}
				//売上ファイルのフォーマットが正しいかどうかの確認
				if(rcdData.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + RCDFILE_INVALID_FORMAT);
					return;
				}
				
				//売上ファイルに記載の支店コードが支店定義ファイルに存在するかどうかの確認
				if(!branchNames.containsKey(rcdData.get(0))) {
					System.out.println(rcdFiles.get(i) .getName() + BRANCH_CODE_NOT_EXIST);
					return;
				}
				//売上ファイルに記載の商品コードが商品定義ファイルに存在するかどうかの確認
				if(!commodityNames.containsKey(rcdData.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_CODE_NOT_EXIST);
					return;
				}
				
				//売上金額が数字かどうか確認
				if(!rcdData.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				
				//売上金額をlongに返還
				long fileSale = Long.parseLong(rcdData.get(2));	;
				
				//抽出した売上額を該当する支店と商品の合計金額にそれぞれ加算
				Long branchSaleAmount = branchSales.get(rcdData.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(rcdData.get(1)) + fileSale;
				
				//売上金額の合計が10桁を超えるかどうかの確認
				long limitDigit = 10000000000L;
				if(branchSaleAmount >= limitDigit || commoditySaleAmount >= limitDigit) {
					System.out.println(OVER_10_DIGITS);
					return;
				}
				
				//売上合計をmapに追加
				branchSales.put(rcdData.get(0), branchSaleAmount);
				commoditySales.put(rcdData.get(1), commoditySaleAmount);
					
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales,
			String regex, String fileError) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			//対象のファイルが存在するかどうかの確認
			if(!file.exists()) {
				System.out.println(fileError + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(fileError + FILE_INVALID_FORMAT);
					return false;
				}
				names.put(items[0], items[1] );
				sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		File file = new File(path, fileName);
		BufferedWriter bw = null;
		
		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}	
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}	
			}
		}
		return true;
	}
}
